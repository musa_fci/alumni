<!DOCTYPE html>
<html>
@include('frontend._partial._head')
  <body>

    <!--Top Header-->
    <section class="top_header"></section>
    <!-- // Top Header-->

    <!--Header-->
      @include('frontend._partial._header')
    <!-- // Header-->

    <!--Menu-->
      @include('frontend._partial._menu')
    <!-- // Menu-->

    <!--Slider-->
      @include('frontend._partial._slider')
    <!-- // Slider-->

      <!--Main Content-->
      @section('content')
      @show
      <!-- // Main Content-->


    <!--Footer-->
    @include('frontend._partial._footer')
    <!-- // Footer-->

@include('frontend._partial._script')

  </body>

</html>
