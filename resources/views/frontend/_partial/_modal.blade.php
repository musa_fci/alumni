<script>
	function loadModalEdit(route, id, edit){
		var baseUrl='<?php echo URL::to(''); ?>';
		$("#body-content").load(baseUrl+"/"+ route +'/'+ id +'/'+ edit);
	}
</script>


<!-- Normal Modal -->
<div id="modal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title" ></h4>
      </div>
      <div class="modal-body" id="body-content">

      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div> <!-- /.modal -->
