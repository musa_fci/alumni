<section class="header">
  <div class="container">
    <div class="row">

      <div class="col-md-6">
        <div class="banner">
          <img src="{{ URL::to('public/frontend/img/logo.png') }}" alt="">
        </div>
      </div>

      <div class="col-md-6">
        <div class="other"></div>
      </div>

    </div>
  </div>
</section>
