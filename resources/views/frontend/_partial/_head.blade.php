<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" type="image/png" sizes="32x32" href="img/favicon-32x32.png">
  <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900" rel="stylesheet">
  <title>Alumni</title>
  <!--Font-Awesome CSS-->
  {!! HTML::style('public/frontend/css/font-awesome.min.css') !!}
  <!--Bootstrap CSS-->
  {!! HTML::style('public/frontend/css/bootstrap.min.css') !!}
  <!-- Owl Carousel CSS-->
  {!! HTML::style('public/frontend/css/owl.carousel.css') !!}
  <!--Main CSS-->
  {!! HTML::style('public/frontend/style.css') !!}

  @section('stylesheet')
  @show

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
