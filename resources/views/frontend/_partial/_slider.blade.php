<section class="slider_section">
  <div class="container">
    <div class="row">

      <div class="col-md-9">
        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
          <!-- Indicators -->
          <ol class="carousel-indicators">
            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
            <li data-target="#carousel-example-generic" data-slide-to="2"></li>
          </ol>

          <!-- Wrapper for slides -->
          <div class="carousel-inner" role="listbox">
            <div class="item active">
              <img src="{{ URL::to('public/frontend/img/slider/slide1.jpg') }}" alt="..."> <!--Slider Images size must be required width:1400px and height:450px -->
            </div>
            <div class="item">
              <img src="{{ URL::to('public/frontend/img/slider/slide2.jpg') }}" alt="..."> <!--Slider Images size must be required width:1400px and height:450px -->
            </div>
            <div class="item">
              <img src="{{ URL::to('public/frontend/img/slider/slide3.jpg') }}" alt="..."> <!--Slider Images size must be required width:1400px and height:450px -->
            </div>
          </div>

          <!-- Controls -->
          <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
        </div>
      </div>

      <div class="col-md-3">
        <div class="login">
          <h3>Alumni Login</h3>
          <form>
            <div class="form-group">
              <div class="input-group">
                <div class="input-group-addon"><span class="fa fa-user-circle"></span> </div>
                <input type="email" class="form-control" id="email" placeholder="Email">
              </div>
            </div>
            <div class="form-group">
              <div class="input-group">
                <div class="input-group-addon"><span class="fa fa-lock"></span></div>
                <input type="password" class="form-control" id="password" placeholder="Password">
              </div>
            </div>
            <button type="submit" class="btn btn-block btn-default">Login</button>
          </form>
        </div>
      </div>

    </div>
  </div>
</section>
