<!--Main JS-->
{!! HTML::script('public/frontend/js/jquery-1.12.4.min.js') !!}
<!--Bootstrap JS-->
{!! HTML::script('public/frontend/js/bootstrap.min.js') !!}
<!-- Owl Carousel JS -->
{!! HTML::script('public/frontend/js/owl.carousel.min.js') !!}
<!--Custome JS-->
{!! HTML::script('public/frontend/js/custom.js') !!}


@section('script')
@show
