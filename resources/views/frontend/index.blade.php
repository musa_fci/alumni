@extends('frontend.app')


@section('content')
<!--Body Content-->
<section class="body_content">
  <div class="container">

    <div class="row">
      <div class="col-md-12 text-center">
        <div class="section_title">
          <h1>About Us</h1>
          <p>WE HAVE EVENTS EVERYWHERE ... WHERE WILL WE SEE YOU?</p>
        </div>
      </div>
    </div>

    <div class="row" style="padding-top: 70px;">
      <div class="col-md-9">
        <div class="left_content">
          <p>
            Daffodil International University (DIU) is recognized in independent government assessments as one of top graded universities in Bangladesh. The university has been founded by Daffodil Group with the approval of the Ministry of Education under the Private University Act of 1992 and its amendment in 1998 and Daffodil International University came into being on 24th January 2002, the University today combines impressive modern facilities and a dynamic approach to teaching and research with its proud heritage of service and achievement.
          </p>
          <p>
            Daffodil International University (DIU) is recognized in independent government assessments as one of top graded universities in Bangladesh. The university has been founded by Daffodil Group with the approval of the Ministry of Education under the Private University Act of 1992 and its amendment in 1998 and Daffodil International University came into being on 24th January 2002, the University today combines impressive modern facilities and a dynamic approach to teaching and research with its proud heritage of service and achievement.
          </p>
        </div>
      </div>

      <div class="col-md-3">
        <div class="alumni_profile">
          <h3>Alumni Profile</h3>

          <div class="alumni_list">

            @if(isset($alumnis))
            @foreach($alumnis as $alumni)
              <div class="single_alumni">
                <a href="#">
                  <img src="{{ URL::to('public/images').'/'.$alumni->picture }}" alt="">
                  <strong>{{ $alumni->name }}</strong>
                  <p>{{ $alumni->designation }}</p>
                  <p>{{ $alumni->organization }}</p>
                </a>
              </div>
            @endforeach
            @endif
          </div>

        </div>
      </div>

    </div>
  </div>
</section>
<!-- // Body Content-->


<!--Events-->
<section class="event_section">
  <div class="container">

    <div class="row">
      <div class="col-md-12 text-center">
        <div class="section_title">
          <h1>Upcoming Events</h1>
          <p>WE HAVE EVENTS EVERYWHERE ... WHERE WILL WE SEE YOU?</p>
        </div>
      </div>
    </div>

    <div class="row event_list">

      <div class="col-md-3">
        <div class="single_event">
          <img src="{{ URL::to('public/frontend/img/event/event-1.jpg') }}" alt="">
          <div class="single_event_inner">
            <div class="event_date">
              <p class="month">July</p>
              <p class="date">26</p>
              <p class="year">2017</p>
            </div>
            <div class="event_title">
              <h3>Daffodil International University - Dallas Reception</h3>
            </div>
          </div>
        </div>
      </div>

      <div class="col-md-3">
        <div class="single_event">
          <img src="{{ URL::to('public/frontend/img/event/event-2.jpg') }}" alt="">
          <div class="single_event_inner">
            <div class="event_date">
              <p class="month">Aug</p>
              <p class="date">15</p>
              <p class="year">2017</p>
            </div>
            <div class="event_title">
              <h3>Daffodil International University - Dallas Reception</h3>
            </div>
          </div>
        </div>
      </div>

      <div class="col-md-3">
        <div class="single_event">
          <img src="{{ URL::to('public/frontend/img/event/event-3.jpg') }}" alt="">
          <div class="single_event_inner">
            <div class="event_date">
              <p class="month">Sep</p>
              <p class="date">20</p>
              <p class="year">2017</p>
            </div>
            <div class="event_title">
              <h3>Daffodil International University - Dallas Reception</h3>
            </div>
          </div>
        </div>
      </div>

      <div class="col-md-3">
        <div class="single_event">
          <img src="{{ URL::to('public/frontend/img/event/event-4.jpg') }}" alt="">
          <div class="single_event_inner">
            <div class="event_date">
              <p class="month">Oct</p>
              <p class="date">01</p>
              <p class="year">2017</p>
            </div>
            <div class="event_title">
              <h3>Daffodil International University - Dallas Reception</h3>
            </div>
          </div>
        </div>
      </div>

    </div>

  </div>
</section>
<!-- // Events-->

<!--Events-->
<section class="notice_section">
  <div class="container">

    <div class="row">
      <div class="col-md-12 text-center">
        <div class="section_title">
          <h1>Latest Notice</h1>
          <p>WE HAVE EVENTS EVERYWHERE ... WHERE WILL WE SEE YOU?</p>
        </div>
      </div>
    </div>

    <div class="row" style="padding-top: 70px;">
      <div class="notice_list">
        <table class="table table-bordered table-hover">
          <tr>
            <th width="5%">#SL</th>
            <th width="75%">Title</th>
            <th width="15%">Publish Date</th>
            <th width="5%">View</th>
          </tr>
          <tr>
            <td>01</td>
            <td>Mid-Term Improvement Exam Schedule, Summer-2017</td>
            <td>2017-07-27</td>
            <td>325</td>
          </tr>
          <tr>
            <td>02</td>
            <td>Mid-Term Improvement Exam Schedule, Summer-2017</td>
            <td>2017-07-27</td>
            <td>200</td>
          </tr>
          <tr>
            <td>03</td>
            <td>Mid-Term Improvement Exam Schedule, Summer-2017</td>
            <td>2017-07-27</td>
            <td>20</td>
          </tr>
          <tr>
            <td>04</td>
            <td>Mid-Term Improvement Exam Schedule, Summer-2017</td>
            <td>2017-07-27</td>
            <td>500</td>
          </tr>
        </table>
      </div>
    </div>

  </div>
</section>
<!-- // Events-->


@endsection
