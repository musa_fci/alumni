<div class="card">
    @include('_partial._success')
    @include('_partial._fail')
    @include('_partial._error')
    <div class="content">
        <form method="post" action="{{ URL::to('admin/alumni') }}{{'/'.$alumni->alumni_id.'/edit'}}" class="form-horizontal" enctype="multipart/form-data">
            <input type="hidden" name="_token" value="{{csrf_token()}}" class="form-control">
            <fieldset>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Category</label>
                    <div class="col-sm-10">
                      <select class="form-control" name="category">
                          <option value="">Select Category</option>

                          @if(isset($cats))
                          @foreach($cats as $cat)
                            <option value="{{$cat->category_name}}" @if($cat->category_name == $alumni->category) selected="selected" @endif>
                              {{$cat->category_name}}
                            </option>
                          @endforeach
                          @endif

                      </select>
                    </div>
                </div>
            </fieldset>
            <fieldset>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Name</label>
                    <div class="col-sm-10">
                        <input type="text" name="name" value="{{ $alumni->name }}" class="form-control">
                    </div>
                </div>
            </fieldset>
            <fieldset>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Organization</label>
                    <div class="col-sm-10">
                        <input type="text" name="organization" value="{{ $alumni->organization }}" class="form-control">
                    </div>
                </div>
            </fieldset>
            <fieldset>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Designation</label>
                    <div class="col-sm-10">
                        <input type="text" name="designation" value="{{ $alumni->designation }}" class="form-control">
                    </div>
                </div>
            </fieldset>
            <fieldset>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Student ID</label>
                    <div class="col-sm-10">
                        <input type="text" name="student_id" value="{{ $alumni->student_id }}" class="form-control">
                    </div>
                </div>
            </fieldset>
            <fieldset>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Passing Year (i.e. 10 - 13)</label>
                    <div class="col-sm-10">
                        <input type="text" name="passing_year" value="{{ $alumni->passing_year }}" class="form-control">
                    </div>
                </div>
            </fieldset>
            <fieldset>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Batch</label>
                    <div class="col-sm-10">
                        <input type="text" name="batch" value="{{ $alumni->batch }}" class="form-control">
                    </div>
                </div>
            </fieldset>
            <fieldset>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Department</label>
                    <div class="col-sm-10">
                        <input type="text" name="department" value="{{ $alumni->department }}" class="form-control">
                    </div>
                </div>
            </fieldset>
            <fieldset>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Mobile Number</label>
                    <div class="col-sm-10">
                        <input type="text" name="mobile" value="{{ $alumni->mobile }}" class="form-control">
                    </div>
                </div>
            </fieldset>
            <fieldset>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Email</label>
                    <div class="col-sm-10">
                        <input type="email" name="email" value="{{ $alumni->email }}" class="form-control">
                    </div>
                </div>
            </fieldset>
            <fieldset>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Password</label>
                    <div class="col-sm-10">
                        <input type="password" name="password" value="{{ $alumni->password }}" class="form-control">
                    </div>
                </div>
            </fieldset>
            <fieldset>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Present Address</label>
                    <div class="col-sm-10">
                        <textarea name="present_address" class="form-control" rows="5">{{ $alumni->present_address }}</textarea>
                    </div>
                </div>
            </fieldset>
            <fieldset>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Permanent Address</label>
                    <div class="col-sm-10">
                        <textarea name="permanent_address" class="form-control" rows="5">{{ $alumni->permanent_address }}</textarea>
                    </div>
                </div>
            </fieldset>
            <fieldset>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Blood Group</label>
                    <div class="col-sm-10">
                      <select class="form-control" name="blood">
                          <option value="">Select Blood Group</option>
                          @if(isset($bloods))
                          @foreach($bloods as $blood)
                            <option value="{{ $blood->blood_group }}" @if ($blood->blood_group == $alumni->blood) selected @endif>{{ $blood->blood_group }}</option>
                          @endforeach
                          @endif
                      </select>
                    </div>
                </div>
            </fieldset>
            <fieldset>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Picture</label>
                    <div class="col-sm-10">
                        <img src="{{URL::to('public/images').'/'.$alumni->picture}}" alt="" style="width:100px;height:100px;">
                        <input type="file" name="picture" class="form-control">
                    </div>
                </div>
            </fieldset>
            <fieldset>
              <div class="form-group">
                <label class="col-sm-2 control-label"></label>
                <div class="col-sm-10">
                  <button type="submit" class="btn btn-fill btn-info">Submit</button>
                </div>
              </div>
            </fieldset>
         </form>
    </div>
</div>  <!-- end card -->
