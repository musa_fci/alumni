@extends('backend.app')

@section('headerTitle','Alumni')

@section('content')

<div class="content">
  <div class="container-fluid">

    <div class="row">
      <div class="col-md-12">
          <div class="card">

              <div class="content content-full-width">

                  <ul role="tablist" class="nav nav-tabs">
                      <li role="presentation" class="{{ !isset($_GET['page']) ? 'active' : '' }}">
                          <a href="#add-blog" data-toggle="tab"><i class="fa fa-user-plus"></i> Add Alumni</a>
                      </li>
                      <li class="{{ isset($_GET['page']) ? 'active' : '' }}">
                          <a href="#blog-list" data-toggle="tab"><i class="fa fa-users"></i> Alumni List</a>
                      </li>
                  </ul>

                  <div class="tab-content">
                      <div id="add-blog" class="tab-pane {{ !isset($_GET['page']) ? 'active' : '' }}">
                        <div class="card">
                            @include('_partial._success')
                            @include('_partial._fail')
                            @include('_partial._error')
                            <div class="content">
                                <form method="post" action="" class="form-horizontal" enctype="multipart/form-data">
                                    <input type="hidden" name="_token" value="{{csrf_token()}}" class="form-control">
                                    <fieldset>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Category</label>
                                            <div class="col-sm-10">
                                              <select class="form-control" name="category">
                                                  <option value="">Select Category</option>
                                                  @if(count($categorys))
                                                  @foreach($categorys as $category)
                                                    <option value="{{ $category->category_name }}">{{ $category->category_name }}</option>
                                                  @endforeach
                                                  @endif
                                              </select>
                                            </div>
                                        </div>
                                    </fieldset>
                                    <fieldset>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Name</label>
                                            <div class="col-sm-10">
                                                <input type="text" name="name" value="{{ old('name') }}" class="form-control">
                                            </div>
                                        </div>
                                    </fieldset>
                                    <fieldset>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Organization</label>
                                            <div class="col-sm-10">
                                                <input type="text" name="organization" value="{{ old('organization') }}" class="form-control">
                                            </div>
                                        </div>
                                    </fieldset>
                                    <fieldset>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Designation</label>
                                            <div class="col-sm-10">
                                                <input type="text" name="designation" value="{{ old('designation') }}" class="form-control">
                                            </div>
                                        </div>
                                    </fieldset>
                                    <fieldset>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Student ID</label>
                                            <div class="col-sm-10">
                                                <input type="text" name="student_id" value="{{ old('student_id') }}" class="form-control">
                                            </div>
                                        </div>
                                    </fieldset>
                                    <fieldset>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Passing Year (i.e. 10 - 13)</label>
                                            <div class="col-sm-10">
                                                <input type="text" name="passing_year" value="{{ old('passing_year') }}" class="form-control">
                                            </div>
                                        </div>
                                    </fieldset>
                                    <fieldset>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Batch</label>
                                            <div class="col-sm-10">
                                                <input type="text" name="batch" value="{{ old('batch') }}" class="form-control">
                                            </div>
                                        </div>
                                    </fieldset>
                                    <fieldset>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Department</label>
                                            <div class="col-sm-10">
                                                <input type="text" name="department" value="{{ old('department') }}" class="form-control">
                                            </div>
                                        </div>
                                    </fieldset>
                                    <fieldset>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Mobile Number</label>
                                            <div class="col-sm-10">
                                                <input type="text" name="mobile" value="{{ old('mobile') }}" class="form-control">
                                            </div>
                                        </div>
                                    </fieldset>
                                    <fieldset>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Email</label>
                                            <div class="col-sm-10">
                                                <input type="email" name="email"  value="{{ old('email') }}" class="form-control">
                                            </div>
                                        </div>
                                    </fieldset>
                                    <fieldset>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Password</label>
                                            <div class="col-sm-10">
                                                <input type="password" name="password"  value="{{ old('password') }}" class="form-control">
                                            </div>
                                        </div>
                                    </fieldset>
                                    <fieldset>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Present Address</label>
                                            <div class="col-sm-10">
                                                <textarea name="present_address" class="form-control" rows="5">{{ old('present_address') }}</textarea>
                                            </div>
                                        </div>
                                    </fieldset>
                                    <fieldset>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Permanent Address</label>
                                            <div class="col-sm-10">
                                                <textarea name="permanent_address" class="form-control" rows="5">{{ old('permanent_address') }}</textarea>
                                            </div>
                                        </div>
                                    </fieldset>
                                    <fieldset>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Blood Group</label>
                                            <div class="col-sm-10">
                                              <select class="form-control" name="blood" value="{{ old('blood') }}">
                                                  <option value="">Select Blood Group</option>
                                                  @if(count($bloods))
                                                  @foreach($bloods as $blood)
                                                    <option value="{{ $blood->blood_group }}">{{ $blood->blood_group }}</option>
                                                  @endforeach
                                                  @endif
                                              </select>
                                            </div>
                                        </div>
                                    </fieldset>
                                    <fieldset>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Picture</label>
                                            <div class="col-sm-10">
                                                <input type="file" name="picture" value="{{ old('picture') }}" class="form-control">
                                            </div>
                                        </div>
                                    </fieldset>
                                    <fieldset>
                                      <div class="form-group">
                                        <label class="col-sm-2 control-label"></label>
                                        <div class="col-sm-10">
                                          <button type="submit" class="btn btn-fill btn-info">Submit</button>
                                        </div>
                                      </div>
                                    </fieldset>
                                 </form>
                            </div>
                        </div>  <!-- end card -->
                      </div>

                      <div id="blog-list" class="tab-pane {{ isset($_GET['page']) ? 'active' : '' }}">
                        <div class="row">
                            <div class="col-md-12">
                              <div class="card">
                                <div class="content table-responsive table-full-width">
                                  <table class="table table-bigboy">
                                    <thead>
                                        <tr>
                                          <th class="text-left">ID</th>
                                          <th class="text-left">Picture</th>
                                          <th class="text-center">Name</th>
                                          <th class="text-center">Student Id</th>
                                            <th class="text-center">Job</th>
                                            <th class="text-center">Contact Info</th>
                                            <th class="text-center">Blood</th>
                                            <th></th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                      @if(isset($alumnis))
                                        <?php $i = 0 ?>
                                      @foreach($alumnis as $alumni)
                                        <?php $i++ ?>
                                        <tr>
                                          <td class="text-left">{{ $alumni->alumni_id }}</td>
                                          <td class="text-left">
                                             <img src="{{URL::to('public/images').'/'.$alumni->picture}}" style="width:50px;height:50px;" alt="...">
                                          </td>
                                          <td class="text-center">{{ $alumni->name }}</td>
                                          <td class="text-center">{{ $alumni->student_id }}</td>
                                          <td class="text-center">{{ $alumni->designation }}<br>at<br>{{ $alumni->organization }}</td>
                                          <td class="text-center">{{ $alumni->email }}<br>and<br>{{ $alumni->mobile }}</td>
                                          <td class="text-center">{{ $alumni->blood }}</td>
                                          <td class="td-actions">
                                            <button type="button" data-toggle="modal" data-target="#myModal-{{ $i }}" rel="tooltip" data-placement="left" title="View" class="btn btn-info btn-simple btn-icon">
                                                <i class="fa fa-image"></i>
                                            </button>
                                            <!--Alumni View Modal -->
                                    				<div class="modal fade bs-example-modal-xs" id="myModal-{{ $i }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                    				  <div class="modal-dialog modal-xs" role="document">
                                    				    <div class="modal-content">
                                    				      <div class="modal-header">
                                    				        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    				        <h4 class="modal-title" id="myModalLabel">Details Information of : {{ $alumni->name }}</h4>
                                    				      </div>
                                    				      <div class="modal-body">
                                                    <table class="table table-bigboy">
                                                      <tr>
                                                        <td colspan="2" style="text-center">
                                                          <img src="{{URL::to('public/images').'/'.$alumni->picture}}" alt="..." style="width:130px;height:130px;margin-bottom:30px;margin-left: 200px;">
                                                        </td>
                                                      </tr>
                                                      <tr>
                                                        <td width="50%" style="padding-left: 100px;">Category</td>
                                                        <td width="50%"><span style="padding-right:20px;">:</span>{{ $alumni->category }}</td>
                                                      </tr>
                                                      <tr>
                                                        <td style="padding-left: 100px;">Organization</td>
                                                        <td><span style="padding-right:20px;">:</span>{{ $alumni->organization }}</td>
                                                      </tr>
                                                      <tr>
                                                        <td style="padding-left: 100px;">Designation</td>
                                                        <td><span style="padding-right:20px;">:</span>{{ $alumni->designation }}</td>
                                                      </tr>
                                                      <tr>
                                                        <td style="padding-left: 100px;">Student Id</td>
                                                        <td><span style="padding-right:20px;">:</span>{{ $alumni->student_id }}</td>
                                                      </tr>
                                                      <tr>
                                                        <td style="padding-left: 100px;">Passing Year</td>
                                                        <td><span style="padding-right:20px;">:</span>{{ $alumni->passing_year }}</td>
                                                      </tr>
                                                      <tr>
                                                        <td style="padding-left: 100px;">Batch</td>
                                                        <td><span style="padding-right:20px;">:</span>{{ $alumni->batch }}</td>
                                                      </tr>
                                                      <tr>
                                                        <td style="padding-left: 100px;">Department</td>
                                                        <td><span style="padding-right:20px;">:</span>{{ $alumni->department }}</td>
                                                      </tr>
                                                      <tr>
                                                        <td style="padding-left: 100px;">Mobile</td>
                                                        <td><span style="padding-right:20px;">:</span>{{ $alumni->mobile }}</td>
                                                      </tr>
                                                      <tr>
                                                        <td style="padding-left: 100px;">Email</td>
                                                        <td><span style="padding-right:20px;">:</span>{{ $alumni->email }}</td>
                                                      </tr>
                                                      <tr>
                                                        <td style="padding-left: 100px;">Blood</td>
                                                        <td><span style="padding-right:20px;">:</span>{{ $alumni->blood }}</td>
                                                      </tr>
                                                      <tr>
                                                        <td style="padding-left: 100px;">Present Address</td>
                                                        <td><span style="padding-right:20px;">:</span>{{ $alumni->present_address }}</td>
                                                      </tr>
                                                      <tr>
                                                        <td style="padding-left: 100px;">Permanent Address</td>
                                                        <td><span style="padding-right:20px;">:</span>{{ $alumni->permanent_address }}</td>
                                                      </tr>
                                                    </table>
                                    				      </div>
                                    				      <div class="modal-footer">
                                    				        <button type="button" class="btn btn-default btn-simple" data-dismiss="modal">Close</button>
                                    				      </div>
                                    				    </div>
                                    				  </div>
                                    				</div>


                                            <!--Alumni Edit Modal -->
                                            <a href="#modal" class="btn btn-success btn-simple btn-icon modal1" onclick="loadModalEdit('admin/alumni','{{$alumni->alumni_id}}','edit')"  data-toggle="modal" type="button" rel="tooltip" data-placement="left" title="Edit">
                                              <i class="fa fa-edit"></i>
                                            </a>
                                            <!--Alumni Delete Button -->
                                            <a href="{{ URL::to('admin/alumni') }}{{'/'.$alumni->alumni_id.'/delete'}}" type="button" rel="tooltip" data-placement="left" title="Remove" class="btn btn-danger btn-simple btn-icon ">
                                                <i class="fa fa-times"></i>
                                            </a>
                                          </td>
                                        </tr>
                                      @endforeach
                                      @endif
                                    </tbody>
                                  </table>
                                    {{ $alumnis->links() }}
                                </div>
                              </div><!--  end card  -->
                            </div> <!-- end col-md-12 -->
                        </div> <!-- end row -->
                      </div>
                  </div>

              </div>
          </div>
      </div>
    </div>

  </div>
</div>

@endsection


@section('script')
<script type="text/javascript">

    // Alumni Edit Modal
    $('.modal1').on('click',function(){
       $('.modal-title').html('Alumni Information Update');
    })


    var $table = $('#bootstrap-table');

    function operateFormatter(value, row, index) {
        return [
            '<a rel="tooltip" title="View" class="btn btn-simple btn-info btn-icon table-action view" href="javascript:void(0)">',
                '<i class="fa fa-image"></i>',
            '</a>',
            '<a rel="tooltip" title="Edit" class="btn btn-simple btn-warning btn-icon table-action edit" href="javascript:void(0)">',
                '<i class="fa fa-edit"></i>',
            '</a>',
            '<a rel="tooltip" title="Remove" class="btn btn-simple btn-danger btn-icon table-action remove" href="javascript:void(0)">',
                '<i class="fa fa-remove"></i>',
            '</a>'
        ].join('');
    }

    $().ready(function(){
        window.operateEvents = {
            'click .view': function (e, value, row, index) {
                info = JSON.stringify(row);

                swal('You click view icon, row: ', info);
                console.log(info);
            },
            'click .edit': function (e, value, row, index) {
                info = JSON.stringify(row);

                swal('You click edit icon, row: ', info);
                console.log(info);
            },
            'click .remove': function (e, value, row, index) {
                console.log(row);
                $table.bootstrapTable('remove', {
                    field: 'id',
                    values: [row.id]
                });
            }
        };

        $table.bootstrapTable({
            toolbar: ".toolbar",
            clickToSelect: true,
            showRefresh: true,
            search: true,
            showToggle: true,
            showColumns: true,
            pagination: true,
            searchAlign: 'left',
            pageSize: 8,
            clickToSelect: false,
            pageList: [8,10,25,50,100],

            formatShowingRows: function(pageFrom, pageTo, totalRows){
                //do nothing here, we don't want to show the text "showing x of y from..."
            },
            formatRecordsPerPage: function(pageNumber){
                return pageNumber + " rows visible";
            },
            icons: {
                refresh: 'fa fa-refresh',
                toggle: 'fa fa-th-list',
                columns: 'fa fa-columns',
                detailOpen: 'fa fa-plus-circle',
                detailClose: 'fa fa-minus-circle'
            }
        });

        //activate the tooltips after the data table is initialized
        $('[rel="tooltip"]').tooltip();

        $(window).resize(function () {
            $table.bootstrapTable('resetView');
        });


    });

    </script>
@endsection
