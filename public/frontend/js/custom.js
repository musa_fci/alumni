jQuery(document).ready(function () {

// Owl Carousel For Alumni Profile
    $('.alumni_list').owlCarousel({
        loop: true,
        responsiveClass: true,
        margin: 5,
        autoplay: 5000,
        nav: false,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 1
            },
            1000: {
                items: 1
            }
        }
    });



});
