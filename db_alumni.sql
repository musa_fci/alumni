-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 03, 2017 at 01:54 PM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_alumni`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_admin`
--

CREATE TABLE `tbl_admin` (
  `admin_id` int(11) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `address` text,
  `status` tinyint(4) DEFAULT '1',
  `remember_token` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_admin`
--

INSERT INTO `tbl_admin` (`admin_id`, `name`, `email`, `password`, `address`, `status`, `remember_token`) VALUES
(1, 'Md. Abu Musa', 'musa@gmail.com', '$2y$10$x4yZPG4KE4nyK5fHlKWnKug66BopeViWGWOZPlwe282Ea1ln3xjB.', 'Sonagazi, Feni', 1, 'tOvLc2UJ56VE1c5B1UQogdQIr1xauY4X6ZdpOq5N9G8KEwOXrge1S6JpvrIT');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_alumni_category`
--

CREATE TABLE `tbl_alumni_category` (
  `category_id` int(11) NOT NULL,
  `category_name` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_alumni_category`
--

INSERT INTO `tbl_alumni_category` (`category_id`, `category_name`) VALUES
(1, 'General'),
(2, 'Associate');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_alumni_registration`
--

CREATE TABLE `tbl_alumni_registration` (
  `alumni_id` int(255) NOT NULL,
  `category` varchar(50) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `organization` varchar(100) DEFAULT NULL,
  `designation` varchar(100) DEFAULT NULL,
  `student_id` varchar(50) DEFAULT NULL,
  `passing_year` varchar(50) DEFAULT NULL,
  `batch` varchar(50) DEFAULT NULL,
  `department` varchar(50) DEFAULT NULL,
  `mobile` varchar(20) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `password` varchar(32) DEFAULT NULL,
  `blood` varchar(5) DEFAULT NULL,
  `present_address` text,
  `permanent_address` text,
  `picture` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_alumni_registration`
--

INSERT INTO `tbl_alumni_registration` (`alumni_id`, `category`, `name`, `organization`, `designation`, `student_id`, `passing_year`, `batch`, `department`, `mobile`, `email`, `password`, `blood`, `present_address`, `permanent_address`, `picture`) VALUES
(11, 'Associate', 'Rafi', 'OrangeBD', 'Developer', '201', '02-03', '12th', 'Civil', '01821325588', 'rokib@gmail.com', '12345', 'B-', 'Qatar', 'sonagazi', '1501700343.jpg'),
(12, 'General', 'Md. Abu Musa', 'BASIS', 'Web Developer', '5555', '32-65', '5th', 'EEE', '987444444', 'musa@gmail.com', '123456', 'O+', 'sfas', 'asdf', '1501753286.jpg'),
(13, 'General', 'alumni', 'BASIS', 'AI Engineer', '5555999999', '02-03', '10th', 'ET', '0185555555', 'admin@gmail.com', '123456', 'O+', 'asdfsada', 'asdfsdf', '1501758552.jpg'),
(14, 'General', 'Md. Abu Musa', 'Unitech Engineer''s Group', 'Web Developer', '98745', '32-65', '5th', 'ET', '0185555555', 'sojib@gmail.com', '123456', 'O-', 'asdfasdfas', 'sadfasdfasf', '1501760250.jpg'),
(15, 'Associate', 'Rafi', 'BASIS', 'Web Developer', '98745', '09-10', '10th', 'CSE', '0185555555', 'mamun@gmail.com', '123456', 'AB+', 'asdfsaf sadf asdf', 'asdfsa dfas fsadf', '1501760321.jpg'),
(16, 'Associate', 'Md. Abu Musa', 'BASIS', 'Web Developer', '5555', '02-03', '5th', 'EEE', '987444444', 'kaka@gmail.com', '654321', 'B+', 'sdfas sadf sfsadf', 'asd fsdf', '1501760419.jpg'),
(17, 'Associate', 'Md. Abu Musa', 'Unitech Engineer''s Group', 'Web Developer', '5555', '09-10', '5th', 'CSE', '987444444', 'rana@gmail.com', '123456', 'A+', 'PRESENT ADDRESS PRESENT ADDRESS PRESENT ADDRESS PRESENT ADDRESS PRESENT ADDRESS PRESENT ADDRESS PRESENT ADDRESS PRESENT ADDRESS PRESENT ADDRESS PRESENT ADDRESS PRESENT ADDRESS PRESENT ADDRESS PRESENT ADDRESS', 'PERMANENT ADDRESSPERMANENT ADDRESSPERMANENT ADDRESSPERMANENT ADDRESSPERMANENT ADDRESSPERMANENT ADDRESSPERMANENT ADDRESSPERMANENT ADDRESSPERMANENT ADDRESSPERMANENT ADDRESS', '1501760676.jpg'),
(18, 'Associate', 'alumni', 'BASIS', 'Web Developer', '080808', '32-65', '10th', 'CSE', '6351635', 'eee@gmail.com', '123456', 'AB+', 'asdsd', 'asfsf', '1501760972.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_blood_group`
--

CREATE TABLE `tbl_blood_group` (
  `blood_id` int(11) NOT NULL,
  `blood_group` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_blood_group`
--

INSERT INTO `tbl_blood_group` (`blood_id`, `blood_group`) VALUES
(1, 'A+'),
(2, 'A-'),
(3, 'B+'),
(4, 'B-'),
(5, 'O+'),
(6, 'O-'),
(7, 'AB+'),
(8, 'AB-');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `tbl_alumni_category`
--
ALTER TABLE `tbl_alumni_category`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `tbl_alumni_registration`
--
ALTER TABLE `tbl_alumni_registration`
  ADD PRIMARY KEY (`alumni_id`);

--
-- Indexes for table `tbl_blood_group`
--
ALTER TABLE `tbl_blood_group`
  ADD PRIMARY KEY (`blood_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
  MODIFY `admin_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_alumni_category`
--
ALTER TABLE `tbl_alumni_category`
  MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tbl_alumni_registration`
--
ALTER TABLE `tbl_alumni_registration`
  MODIFY `alumni_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `tbl_blood_group`
--
ALTER TABLE `tbl_blood_group`
  MODIFY `blood_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
