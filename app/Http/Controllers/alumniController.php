<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Lang;
use File;


class alumniController extends Controller
{
    /*******************************/
    /********Alumni Index***********/
    /*******************************/

    public function alumni()
    {
      $alumnis   = DB::table('tbl_alumni_registration')->paginate(10);
      $categorys = DB::table('tbl_alumni_category')->get();
      $bloods    = DB::table('tbl_blood_group')->get();
      return view('backend.alumni',compact('alumnis','categorys','bloods'));
    }


    /*******************************/
    /******Alumni Registration******/
    /*******************************/

    public function alumniRegistration(Request $request)
    {

      $this->validate($request,[
        'category'            =>       'required',
        'name'                =>       'required',
        'organization'        =>       'required',
        'designation'         =>       'required',
        'student_id'          =>       'required',
        'passing_year'        =>       'required',
        'batch'               =>       'required',
        'department'          =>       'required',
        'mobile'              =>       'required|',
        'email'               =>       'required|email|unique:tbl_alumni_registration',
        'password'            =>       'required|min:6|max:12',
        'present_address'     =>       'required',
        'permanent_address'   =>       'required',
        'blood'               =>       'required',
        'picture'             =>       'required',

      ]);

      try {
        $picture = time().'.'.$request->picture->getClientOriginalExtension();

        DB::table('tbl_alumni_registration')->insert([
          'category'            =>       $request->category,
          'name'                =>       $request->name,
          'organization'        =>       $request->organization,
          'designation'         =>       $request->designation,
          'student_id'          =>       $request->student_id,
          'passing_year'        =>       $request->passing_year,
          'batch'               =>       $request->batch,
          'department'          =>       $request->department,
          'mobile'              =>       $request->mobile,
          'email'               =>       $request->email,
          'password'            =>       $request->password,
          'present_address'     =>       $request->present_address,
          'permanent_address'   =>       $request->permanent_address,
          'blood'               =>       $request->blood,
          'picture'             =>       $picture,
        ]);

        $request->picture->move(public_path('images'),$picture);
        return back()->with('success','Alumni Registration Successfully');

      } catch (\Exception $e) {
        return back()->with('fail',Lang::get('DBerror.'.$e->errorInfo[1]));
      }
    }

    /*******************************/
    /*****Alumni Remove/Delete******/
    /*******************************/
    public function alumniDelete($id = null)
    {
      try {
        $pictures = DB::table('tbl_alumni_registration')->select('picture')->where('alumni_id',$id)->get();
        foreach ($pictures as $picture) {
          $picturePath = public_path('images/').$picture->picture;
          File::delete($picturePath);
        }
        DB::table('tbl_alumni_registration')->where('alumni_id',$id)->delete();
        return back()->with('success','Alumni Delete Successfully');

      } catch (\Exception $e) {
        return back()->with('fail',Lang::get('DBerror.'.$e->errorInfo[1]));
      }

    }


    /*******************************/
    /*****Alumni Update/Edit********/
    /*******************************/
    public function AlumniSelectById($id = null)
    {
      $alumni = DB::table('tbl_alumni_registration')->where('alumni_id',$id)->first();
      $cats   = DB::table('tbl_alumni_category')->get();
      $bloods = DB::table('tbl_blood_group')->get();
      return view('backend.loadAlumniEdit',compact('alumni','cats','bloods'));
    }


    public function AlumniUpdate($id = null, Request $request)
    {
      try {
        if (!empty($request->picture)) {
          $pictures = DB::table('tbl_alumni_registration')->select('picture')->where('alumni_id',$id)->get();
          foreach ($pictures as $picture) {
            $picturePath = public_path('images/').$picture->picture;
            File::delete($picturePath);
          }

          $picture = time().'.'.$request->picture->getClientOriginalExtension();

          DB::table('tbl_alumni_registration')->where('alumni_id',$id)->update([
            'category'            =>       $request->category,
            'name'                =>       $request->name,
            'organization'        =>       $request->organization,
            'designation'         =>       $request->designation,
            'student_id'          =>       $request->student_id,
            'passing_year'        =>       $request->passing_year,
            'batch'               =>       $request->batch,
            'department'          =>       $request->department,
            'mobile'              =>       $request->mobile,
            'email'               =>       $request->email,
            'password'            =>       $request->password,
            'present_address'     =>       $request->present_address,
            'permanent_address'   =>       $request->permanent_address,
            'blood'               =>       $request->blood,
            'picture'             =>       $picture,
          ]);

          $request->picture->move(public_path('images'),$picture);
          return back()->with('success','Alumni Information Update Successfully');

        }else{
          DB::table('tbl_alumni_registration')->where('alumni_id',$id)->update([
            'category'            =>       $request->category,
            'name'                =>       $request->name,
            'organization'        =>       $request->organization,
            'designation'         =>       $request->designation,
            'student_id'          =>       $request->student_id,
            'passing_year'        =>       $request->passing_year,
            'batch'               =>       $request->batch,
            'department'          =>       $request->department,
            'mobile'              =>       $request->mobile,
            'email'               =>       $request->email,
            'password'            =>       $request->password,
            'present_address'     =>       $request->present_address,
            'permanent_address'   =>       $request->permanent_address,
            'blood'               =>       $request->blood,
          ]);

          return back()->with('success','Alumni Information Update Successfully');
        }
      } catch (\Exception $e) {
        return back()->with('fail',Lang::get('DBerror.'.$e->errorInfo[1]));
      }

    }


}
