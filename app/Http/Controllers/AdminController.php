<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Hash;
use Auth;
use Session;;

class AdminController extends Controller
{

    public function login()
    {
      return view('backend.login');
    }


    public function Checklogin(Request $request)
    {
      $this->validate($request,[
        'email'       =>    'required',
        'password'    =>    'required',
      ]);

      $credential = ['email'=> $request->email, 'password'=>$request->password];
      if(Auth::attempt($credential)){
        Session::put('admin',Auth::user());
        return redirect('admin')->with('success','Login Success');
      }else{
        return redirect('admin/login')->with('fail','Login Information Is Wrong...!');
      }

    }


    public function logout()
    {
      Auth::logout();
      Session::forget('admin');
      return redirect('admin/login')->with('success','Successfully Logout.');
    }




}
