<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use File;
use Lang;

class frontendController extends Controller
{
    public function index()
    {
      $alumnis = DB::table('tbl_alumni_registration')->get();
      return view('frontend/index',compact('alumnis'));
    }
}
