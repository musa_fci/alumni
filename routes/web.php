<?php

/*******************************/
/********Frontend Start*********/
/*******************************/

Route::get('/',[
  'as'    =>    '/',
  'uses'  =>    'frontendController@index'
]);





/*******************************/
/*********Backend Start*********/
/*******************************/
//
// Route::get('register',[
//     'as'    => 'register',
//     'uses'  => 'UserController@register'
// ]);
//
// Route::post('register',[
//     'as'    => 'register',
//     'uses'  => 'UserController@userRegister'
// ]);
//
// Route::get('login',[
//     'as'     =>  'login',
//     'uses'   =>  'UserController@login'
// ]);
//
// Route::post('login',[
//     'as'     =>  'login',
//     'uses'   =>  'UserController@checkLogin'
// ]);
//
// Route::get('logout',[
//   'as'    =>  'logout',
//   'uses'  =>  'UserController@logout'
// ]);


// Route::group(['middleware' => ['UserAuth']],function(){

  //Start Blog Operation
  Route::get('blog',[
      'as'    => 'blog',
      'uses'  => 'BlogController@blog'
  ]);

  Route::post('blog',[
      'as'    => 'blog',
      'uses'  => 'BlogController@blogStore'
  ]);

  Route::get('blog/{id}/delete',[
    'as'    =>  'delete',
    'uses'  =>  'BlogController@blogDelete'
  ]);

  Route::post('blog/{id}/edit',[
    'as'    =>  'edit',
    'uses'  =>  'BlogController@blogUpdate'
  ]);
  //End Blog Operation




  //Start Alumni Operation
  Route::group(['middleware'=>'AdminAuth'],function () {

    Route::get('admin', function () {
        return view('backend.index');
    });



    Route::prefix('admin')->group(function () {

      Route::get('alumni',[
        'as'    =>  'alumni',
        'uses'  =>  'alumniController@alumni'
      ]);

      Route::post('alumni',[
        'as'    =>  'alumni',
        'uses'  =>  'alumniController@alumniRegistration'
      ]);

      Route::get('alumni/{id}/delete',[
        'as'    =>   'delete',
        'uses'  =>   'alumniController@alumniDelete'
      ]);

      Route::get('alumni/{id}/edit',[
        'as'    =>    'edit',
        'uses'  =>    'alumniController@AlumniSelectById'
      ]);

      Route::post('alumni/{id}/edit',[
        'as'    =>    'update',
        'uses'  =>    'alumniController@AlumniUpdate'
      ]);

    });

  });
  //Close Alumni Operation


  //Start Admin Login Operation
  Route::prefix('admin')->group(function(){

    Route::get('login',[
      'as'    =>    'login',
      'uses'  =>    'AdminController@login'
    ]);

    Route::post('login',[
      'as'    =>    'login',
      'uses'  =>    'AdminController@Checklogin'
    ]);

    Route::get('logout',[
      'as'    =>    'logout',
      'uses'  =>    'AdminController@logout'
    ]);

  });
  //Close Admin Login Operation

Route::get('admin/charts', function () {
    return view('backend.charts');
});

Route::get('admin/calendar', function () {
    return view('backend.calendar');
});

//components
Route::get('admin/buttons',function(){
  return view('backend.components.buttons');
});
Route::get('admin/grid',function(){
  return view('backend.components.grid');
});
Route::get('admin/icons',function(){
  return view('backend.components.icons');
});
Route::get('admin/notifications',function(){
  return view('backend.components.notifications');
});
Route::get('admin/panels',function(){
  return view('backend.components.panels');
});
Route::get('admin/sweet-alert',function(){
  return view('backend.components.sweet-alert');
});
Route::get('admin/typography',function(){
  return view('backend.components.typography');
});

//forms
Route::get('admin/form-regular',function(){
  return view('backend.forms.regular');
});
Route::get('admin/form-extended',function(){
  return view('backend.forms.extended');
});
Route::get('admin/form-validation',function(){
  return view('backend.forms.validation');
});
Route::get('admin/form-wizard',function(){
  return view('backend.forms.wizard');
});


//tables
Route::get('admin/table-regular',function(){
  return view('backend.tables.regular');
});
Route::get('admin/table-extended',function(){
  return view('backend.tables.extended');
});
Route::get('admin/table-bootstrap',function(){
  return view('backend.tables.bootstrap-table');
});
Route::get('admin/table-datatables',function(){
  return view('backend.tables.datatables');
});

//maps
Route::get('admin/google-map',function(){
  return view('backend.maps.google');
});
Route::get('admin/vector-map',function(){
  return view('backend.maps.vector');
});
Route::get('admin/fullscreen-map',function(){
  return view('backend.maps.fullscreen');
});

//Other pages
Route::get('admin/user',function(){
  return view('backend.pages.user');
});



// });
